# Generative Christmas cards

![Screenshot of the editor showing circles which wobble when moving the cursor over them.](./docs/screenshot-editor.png)

![Screenshot of the gallery showing cropped examples of the initial circles in various wobble states.](./docs/screenshot-gallery.png)

This project is a commission for [Buck et Baumgärtel](https://www.bb-ulm.eu). While the source code is licensed under MIT, the Buck et Baumgärtel logo is not free. A free substitute can be found at `assets/images/logo.svg`.

## Usage
* Hold `Space` or hold the camera icon to slow down the animation.
* Press `S` or click the camera icon to take a snapshot.
* Press `G` or click the gallery icon to go to the gallery.

## Development
### Install the dependencies
This project gets transpiled with ParcelJS, which is a NodeJS application. Please install NodeJS, if you don't have it already.

Then use NodeJS' package manager `npm` to install the dependencies of this project. Run the following command in the folder of this project:
```bash
npm install
```

### Build the page
```bash
npm run start  # Starts a development server at localhost:1234
npm run build  # Builds the production version to ./dist
npm run watch  # Builds the production version to ./dist and updates it on source changes
```
