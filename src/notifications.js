import './notifications.scss';

export const spawnNotification = ({text}) => {
  const notification = document.createElement('div');
  notification.innerText = text;
  document.getElementById('notifications').appendChild(notification);
  // Set a timeout to remove fade out the notification.
  setTimeout(() => {
    notification.classList.add('fade-out');
    // Remove it from the DOM, after it has been removed.
    setTimeout(() => {
      notification.remove();
    }, 2000);
  }, 12000);
};
