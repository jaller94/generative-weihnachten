function transformPosition(state, [x, y]) {
  const t = state.time / 0.2;
  const t2 = state.time / 0.143;
  const t3 = state.time / 0.092;
  //const t4 = state.time / 0.175;
  //const t5 = state.time / 0.127;
  const factor = Math.sqrt(Math.sqrt(Math.pow(x,2) + Math.pow(y,2)));
  const xChange = Math.sin(x * state.wobbleFrequencyVertical + y * state.waveFrequencyVertical + t * state.speedVertical) * state.horizontalStrength + Math.sin(y * state.wobbleFrequencyVertical * 0.8 + x * -state.waveFrequencyVertical * 0.75 + t3 * state.speedVertical) * state.horizontalStrength * 0.7;
  const yChange = Math.sin(y * state.wobbleFrequencyHorizontal + x * state.waveFrequencyHorizontal + t2 * state.speedHorizontal) * state.verticalStrength;
  return [x + xChange * factor, y + yChange * factor];
}

export const draw = (ctx, state, steps = 50) => {
  const computedStyle = getComputedStyle(ctx.canvas);
  ctx.fillStyle = computedStyle.getPropertyValue('--primary-color');
  let radius = 1;
  
  // Draw 40 circles from the outside to the inside.
  // Every second circle gets cleared, resulting in 20 paths the user will
  // perceive.
  for (let i = 40; i > 0; i--) {
    // The default radius at which the currect path is drawn before the
    // transformation. This results in the paths being thicker in the center.
    radius -= i%2 === 0 ? 0.01 + (i / 40 * 0.02) : 0.04 - (i / 40 * 0.032);
    ctx.beginPath();
    for(let i = 0; i <= steps; i++) {
      const deg = i / steps * 360;
      const x = Math.sin(deg / 180 * Math.PI) * radius;
      const y = Math.cos(deg / 180 * Math.PI) * radius;
      const transformedPosition = transformPosition(state, [x, y]);
      ctx.lineTo(transformedPosition[0], transformedPosition[1]);
    }
    if (i%2 === 0) {
      ctx.fill();
    } else {
      ctx.save();
      ctx.clip();
      ctx.clearRect(-2, -2, 4, 4);
      ctx.restore();
    }
  }
};
