import { loadFromLocalStorage } from './globals';
loadFromLocalStorage();
import { lineToObject } from './links';
import { spawnNotification } from './notifications';

import { showEditor, hideEditor } from './views/editor';
import { showGallery, hideGallery } from './views/gallery';
import { showSettings, hideSettings } from './views/settings';
import { showViewer, hideViewer } from './views/viewer';

//
// import { html, render } from 'lit-html';
//
// const helloTemplate = (name) => html`<div>Hello ${name}!</div>`;
// render(helloTemplate('Kevin'), document.getElementById('button-camera'));

const onKeyPress = (event) => {
  const key = event.key.toLowerCase();
  if (key === 'g') {
    window.location = '#gallery';
  }
};

window.addEventListener('keypress', onKeyPress);

let hideCurrent;
const route = () => {
  if (hideCurrent) hideCurrent();
  // Delay this a bit, so pointer events don't carry over
  setTimeout(showNewPage, 100);
};
const showNewPage = () => {
  if (document.location.hash.slice(1) === 'editor') {
    showEditor();
    hideCurrent = hideEditor;
  } else if (document.location.hash.slice(1) === 'gallery') {
    showGallery();
    hideCurrent = hideGallery;
  } else if (document.location.hash.slice(1) === 'settings') {
    showSettings();
    hideCurrent = hideSettings;
  } else if (document.location.hash.slice(1).startsWith('card')) {
    let state;
    try {
      state = lineToObject(document.location.hash.slice(6));
    } catch(error) {
      console.error(error);
    }
    if (state) {
      window.snapshot = state;
      showViewer();
      hideCurrent = hideViewer;
    }
  } else {
    window.location = '#editor';
    // Welcome
    setTimeout(() => {
      spawnNotification({text: 'Wische über die Kreise,\num Wellen zu erzeugen.'});
    }, 2000);

    setTimeout(() => {
      spawnNotification({text: 'Halte den Foto-Button,\num die Animation zu verlangsamen.'});
    }, 17000);

    setTimeout(() => {
      spawnNotification({text: 'Drücke den Foto-Button,\num eine Aufnahme anzufertigen.'});
    }, 32000);
  }
};

addEventListener('hashchange', route);
route();
