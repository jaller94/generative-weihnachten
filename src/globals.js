// Supported features: Does the browser support it?
window.featureColors = window.CSS && CSS.supports('color', 'var(--fake-var)');
window.featureGallery = true;

export const loadFromLocalStorage = () => {
  if (!window.localStorage) {
    console.warn('localStorage not supported: Cannot load preferences');
    return;
  }
  if (!window.JSON) {
    console.warn('JSON not supported: Cannot load preferences');
    return;
  }
  window.gallery = JSON.parse(
    window.localStorage.getItem('generative-weihnachten-gallery') || '[]'
  );
  window.gallery.forEach((item) => {
    if (item.area !== undefined) {
      item.x = item.area % 4 * 0.4;
      item.y = Math.floor(item.area / 4) * 0.4;
      item.width = 0.65;
      item.height = 0.65;
      delete item.area;
    }
  });
  const data = JSON.parse(
    window.localStorage.getItem('generative-weihnachten') || '{}'
  );
  // Preferences: Does the user want it?
  window.enableAnimations = data.enableAnimations || true;
  window.enableDarkMode = data.enableDarkMode || true;

  // The user's progress
  window.knowsSwipe = data.knowsSwipe || false;
  window.knowsSlowMotion = data.knowsSlowMotion || false;
  window.knowsPhotos = data.knowsPhotos || false;
  window.knowsDownload = data.knowsDownload || false;
  window.knowsGallery = data.knowsGallery || false;
};

export const saveToLocalStorage = () => {
  if (!window.localStorage) {
    console.warn('localStorage not supported: Cannot save preferences');
    return;
  }
  if (!window.JSON) {
    console.warn('JSON not supported: Cannot save preferences');
    return;
  }
  window.localStorage.setItem('generative-weihnachten-gallery', JSON.stringify(
    window.gallery.slice(-100)
  ));
  window.localStorage.setItem('generative-weihnachten', JSON.stringify({
    enableAnimations: window.enableAnimations,
    enableDarkMode: window.enableDarkMode,
    
    knowsSwipe: window.knowsSwipe,
    knowsSlowMotion: window.knowsSlowMotion,
    knowsPhotos: window.knowsPhotos,
    knowsDownload: window.knowsDownload,
    knowsGallery: window.knowsGallery,
  }));
};
