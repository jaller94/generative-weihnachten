import _debounce from 'lodash.debounce';

import { objectToLine } from '../links';
import { draw } from '../visuals';

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

const randomFloat = (min, max) => {
  return Math.random() * (max - min) + min;
};

export const newState = () => ({
  time: 0,
  verticalStrength: 0, // 0 to 0.2
  speedVertical: roundToPrecision(randomFloat(-2, 2), 0.001), // -2 to 2
  waveFrequencyVertical: roundToPrecision(randomFloat(-5, 5), 0.001), // -10 to 10
  wobbleFrequencyVertical: roundToPrecision(randomFloat(-5, 5), 0.001), // -7 to 7
  horizontalStrength: 0, // 0 to 0.2
  speedHorizontal: roundToPrecision(randomFloat(-2, 2), 0.001), // -2 to 2
  waveFrequencyHorizontal: roundToPrecision(randomFloat(-5, 5), 0.001), // -10 to 10
  wobbleFrequencyHorizontal: roundToPrecision(randomFloat(-5, 5), 0.001), // -7 to 7
});

export const reset = () => {
  state = newState();
  xPointer = null;
  yPointer = null;
};

function fall(value, rate, preferred) {
  if (value > preferred) {
    return Math.max(preferred, value - rate);
  } else {
    return Math.min(preferred, value + rate);
  }
}

function change(value, rate, min, max) {
  return Math.min(max, Math.max(min, value + rate));
}

function animate(delta, state) {
  return {
    ...state,
    verticalStrength: fall(state.verticalStrength, 0.007 * delta, 0),
    // speedVertical: change(state.speedVertical, (Math.random() - 0.5) * delta, -2, 2),
    // waveFrequencyVertical: change(state.speedVertical, (Math.random() - 0.5) * delta, -5, 5),
    // wobbleFrequencyVertical: change(state.speedVertical, (Math.random() - 0.5) * delta, -5, 5),
    horizontalStrength: fall(state.horizontalStrength, 0.007 * delta, 0),
    // speedHorizontal: fall(state.speedHorizontal, 0.001 * delta, 0),
    // waveFrequencyHorizontal: fall(state.waveFrequencyHorizontal, 0.001 * delta, 0),
    // wobbleFrequencyHorizontal: fall(state.wobbleFrequencyHorizontal, 0.001 * delta, 0),
  };
}

let lastTimestamp = 0;
let state, xPointer, yPointer;
reset();
/* Main Loop */
function step(timestamp) {
  ctx.beginPath();
  ctx.rect(-2, -2, 4, 4);
  ctx.clip();
  ctx.clearRect(-2, -2, 4, 4);
  draw(ctx, state);
  //const delta = Math.max(0.015, (timestamp - lastTimestamp) / 1000);
  let delta = (timestamp - lastTimestamp) / 1000;
  if (window.slowMotionOn) delta /= 10;
  lastTimestamp = timestamp;
  state.time += delta;

  state = animate(delta, state);

  waitingOnFrame = false;
  if (animationRunning && window.enableAnimations) {
    requestFrame();
  }
}

function resizeCanvas() {
  const newLength = canvas.clientWidth;
  canvas.width = canvas.height = newLength;
  // reset transforms
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  // set zoom and view
  ctx.scale(newLength / 2.6, newLength / 2.6);
  ctx.translate(1.3, 1.3);
}

const debouncedStep = _debounce(() => {
  requestFrame();
}, 300);

function dragging(event) {
  const pointer = event.touches ? event.touches[0] : event;

  if (xPointer !== null && yPointer !== null) {
    const xDist = (pointer.clientX - xPointer) / canvas.width / 2;
    const yDist = (pointer.clientY - yPointer) / canvas.width / 2;
    state = {
      ...state,
      horizontalStrength: change(state.horizontalStrength, Math.abs(xDist) * 0.07, 0, 0.2),
      //speedHorizontal: change(state.speedHorizontal, (Math.random() - 0.5) * 0.01, -2, 2),
      //wobbleFrequencyHorizontal: change(state.wobbleFrequencyHorizontal, (Math.random() - 0.5) * 0.01, -10, 10),
      verticalStrength: change(state.verticalStrength, Math.abs(yDist) * 0.07, 0, 0.2),
      //speedVertical: change(state.speedVertical, yDist * -0.01, -2, 2),
      //wobbleFrequencyVertical: change(state.wobbleFrequencyVertical, yDist * -0.01, -10, 10),
    };
  }

  // Update the values for the next time
  xPointer = pointer.clientX;
  yPointer = pointer.clientY;

  if (!window.enableAnimations) {
    debouncedStep();
  }
}

let waitingOnFrame = false;
export const requestFrame = () => {
  if (waitingOnFrame) return;
  waitingOnFrame = true;
  window.requestAnimationFrame(step);
};

let animationRunning = true;
export const startAnimation = (running) => {
  animationRunning = running;
  if (running) {
    requestFrame();
  }
};

const onWindowResize = () => {
  resizeCanvas();
  requestFrame();
};
const isSpaceKey = (key) => key === ' ' || key === 'spacebar';

const onKeyPress = (event) => {
  const key = event.key.toLowerCase();
  if (key === 'r') {
    reset();
  }
  if (key === 's') {
    takeSnapshot();
  }
};

const onKeyDown = (event) => {
  const key = event.key.toLowerCase();
  if (isSpaceKey(key)) {
    window.slowMotionOn = true;
  }
};

const onKeyUp = (event) => {
  const key = event.key.toLowerCase();
  if (isSpaceKey(key)) {
    window.slowMotionOn = false;
  }
};

// const changeColors = (primaryColor, backgroundColor) => {
//   window.primaryColor = primaryColor;
//   window.backgroundColor = backgroundColor;
//   document.documentElement.style.setProperty('--primary-color', primaryColor);
//   document.documentElement.style.setProperty('--background-color', backgroundColor);
// };

// changeColors('red', 'darkgreen');

const cameraButton = document.getElementById('button-photo');
cameraButton.addEventListener('pointerdown', () => {
  window.slowMotionOn = true;
});
window.addEventListener('pointerup', () => {
  window.slowMotionOn = false;
});
cameraButton.addEventListener('pointerup', () => {
  takeSnapshot();
});

function roundToPrecision(x, precision) {
  var y = +x + (precision === undefined ? 0.5 : precision/2);
  return y - (y % (precision === undefined ? 1 : +precision));
}

const takeSnapshot = () => {
  const area = Math.floor(Math.random() * 16);
  const card = {
    ...state,
    time: roundToPrecision(state.time, 0.001),
    x: (area % 4 * 0.4),
    y: (Math.floor(area / 4) * 0.4),
    width: 0.65,
    height: 0.65,
  };
  console.log(card);
  window.location = `#card=${objectToLine(card)}`;
};

export const showEditor = () => {
  document.getElementById('canvas').classList.toggle('hide', false);
  // document.getElementById('quote').classList.toggle('hide', false);
  document.getElementById('button-photo').classList.toggle('hide', false);
  document.getElementById('button-gallery').classList.toggle('hide', false);

  canvas.addEventListener('mousemove', dragging);
  canvas.addEventListener('touchmove', dragging);
  window.addEventListener('resize', onWindowResize);
  window.addEventListener('keypress', onKeyPress);
  window.addEventListener('keydown', onKeyDown);
  window.addEventListener('keyup', onKeyUp);

  xPointer = null;
  yPointer = null;

  resizeCanvas();
  startAnimation(true);
};

export const hideEditor = () => {
  canvas.removeEventListener('mousemove', dragging);
  canvas.removeEventListener('touchmove', dragging);
  window.removeEventListener('resize', onWindowResize);
  window.removeEventListener('keypress', onKeyPress);
  window.removeEventListener('keydown', onKeyDown);
  window.removeEventListener('keyup', onKeyUp);

  startAnimation(false);

  document.getElementById('canvas').classList.toggle('hide', true);
  // document.getElementById('quote').classList.toggle('hide', true);
  document.getElementById('button-photo').classList.toggle('hide', true);
  document.getElementById('button-gallery').classList.toggle('hide', true);
};
