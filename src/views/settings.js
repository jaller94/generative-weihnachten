import './settings.scss';

export const showSettings = () => {
  document.getElementById('settings').classList.toggle('hide', false);
};

export const hideSettings = () => {
  document.getElementById('settings').classList.toggle('hide', true);
};
