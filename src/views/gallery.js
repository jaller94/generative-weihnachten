import { objectToLine } from '../links';
import { draw } from '../visuals';
// import { newState } from './editor';

import './gallery.scss';

// if (window.gallery.length === 0) {
//   for (let i = 0; i < 2; i++) {
//     const state = newState();
//     state.horizontalStrength = Math.random() * 0.4 - 0.2;
//     state.verticalStrength = Math.random() * 0.4 - 0.2;
//     state.area = i * 2 + 7;
//     window.gallery.push(state);
//   }
// }

export const showGallery = () => {
  window.gallery.forEach(addItem);
  document.getElementById('quote').classList.toggle('hide', true);
  document.getElementById('gallery').classList.toggle('hide', false);
  document.getElementById('button-back').classList.toggle('hide', false);
  document.getElementById('button-gallery').classList.toggle('hide', true);
  document.getElementById('button-back').addEventListener('click', hideGallery);
};

export const hideGallery = () => {
  document.getElementById('gallery-items').innerText = '';
  document.getElementById('quote').classList.toggle('hide', false);
  document.getElementById('gallery').classList.toggle('hide', true);
  document.getElementById('button-back').classList.toggle('hide', true);
  document.getElementById('button-gallery').classList.toggle('hide', false);
  document.getElementById('button-back').removeEventListener('click', hideGallery);
};

const addItem = (item) => {
  const node = document.createElement('li');
  node.classList.add('gallery__item');
  const link = document.createElement('a');
  link.href = `#card=${objectToLine(item)}`;
  node.appendChild(link);
  const canvas = document.createElement('canvas');
  canvas.width = canvas.height = 256;
  link.appendChild(canvas);
  const galleryList = document.getElementById('gallery-items');
  galleryList.insertBefore(node, galleryList.firstItem);
  setTimeout(() => {
    const ctx = canvas.getContext('2d');

    // reset transforms
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.scale(canvas.width / item.width, canvas.height / item.height);
    const xTranslate = 1 - item.x;
    const yTranslate = 1 - item.y;
    // console.log(xTranslate, yTranslate)
    ctx.translate(xTranslate, yTranslate);
    draw(ctx, item, 360);
  }, 15);
};
