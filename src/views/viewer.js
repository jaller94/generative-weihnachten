import { saveToLocalStorage } from '../globals';
import { draw } from '../visuals';

import './viewer.scss';

const isInGallery = (needle) => {
  return window.gallery.find((item) => {
    return (
      item.time === needle.time && 
      item.x === needle.x &&
      item.y === needle.y &&
      item.width === needle.width &&
      item.height === needle.height &&
      item.verticalStrength === needle.verticalStrength &&
      item.horizontalStrength === needle.horizontalStrength
    );
  });
};

export const showViewer = () => {
  document.getElementById('output').classList.toggle('hide', false);
  document.getElementById('output').classList.toggle('flashAnimation', true);
  document.getElementById('button-back').classList.toggle('hide', false);
  document.getElementById('button-gallery').classList.toggle('hide', false);
  document.getElementById('button-download').classList.toggle('hide', false);
  document.getElementById('button-back').addEventListener('click', hideViewer);

  const canvas = document.getElementById('output');
  const ctx = canvas.getContext('2d');

  ctx.clearRect(-2, -2, 4, 4);

  const item = window.snapshot;

  // reset transforms
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.scale(canvas.width / item.width, canvas.height / item.height);
  const xTranslate = 1 - item.x;
  const yTranslate = 1 - item.y;
  // console.log(xTranslate, yTranslate)
  ctx.translate(xTranslate, yTranslate);

  draw(ctx, item, 360);
  const downloadButton = document.getElementById('button-download');
  downloadButton.setAttribute('href', canvas.toDataURL('image/png'));
  downloadButton.setAttribute('download', `BuckEtBaumgärtel_Card_${new Date().getTime()}.png`);
  
  // Save the new entry to the gallery
  if (!isInGallery(window.snapshot)) {
    window.gallery.push(window.snapshot);
    console.log('Added to the gallery');
    saveToLocalStorage();
  }
};

export const hideViewer = () => {
  document.getElementById('output').classList.toggle('hide', true);
  document.getElementById('output').classList.toggle('flashAnimation', false);
  document.getElementById('button-back').classList.toggle('hide', true);
  document.getElementById('button-download').classList.toggle('hide', true);
  document.getElementById('button-back').removeEventListener('click', hideViewer);

  const downloadButton = document.getElementById('button-download');
  downloadButton.setAttribute('href', '');
  downloadButton.setAttribute('download', '');
};
