//version,time,x,y,width,height,verticalStrength,speedVertical,waveFrequencyVertical,wobbleFrequencyVertical,horizontalStrength,speedHorizontal,waveFrequencyHorizontal,wobbleFrequencyHorizontal

export const lineToObject = (input) => {
  const params = input.split(',');
  if (params[0] !== '1') throw new Error(`Unknown version number: ${params[0]}.`);
  if (params.length !== 14) throw new Error(`A link must have 14 parameters. Found ${params.length}.`);
  const object = {
    time: Number.parseFloat(params[1]),
    x: Number.parseFloat(params[2]),
    y: Number.parseFloat(params[3]),
    width: Number.parseFloat(params[4]),
    height: Number.parseFloat(params[5]),
    verticalStrength: Number.parseFloat(params[6]),
    speedVertical: Number.parseFloat(params[7]),
    waveFrequencyVertical: Number.parseFloat(params[8]),
    wobbleFrequencyVertical: Number.parseFloat(params[9]),
    horizontalStrength: Number.parseFloat(params[10]),
    speedHorizontal: Number.parseFloat(params[11]),
    waveFrequencyHorizontal: Number.parseFloat(params[12]),
    wobbleFrequencyHorizontal: Number.parseFloat(params[13]),
  };
  return object;
}; 

export const objectToLine = (input) => {
  return '1,'
    + input.time + ','
    + input.x + ','
    + input.y + ','
    + input.width + ','
    + input.height + ','
    + input.verticalStrength + ','
    + input.speedVertical + ','
    + input.waveFrequencyVertical + ','
    + input.wobbleFrequencyVertical + ','
    + input.horizontalStrength + ','
    + input.speedHorizontal + ','
    + input.waveFrequencyHorizontal + ','
    + input.wobbleFrequencyHorizontal;
}; 
